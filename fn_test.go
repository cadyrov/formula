package formula

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBracers(t *testing.T) {
	ass := assertsBracers()

	for i := range ass {
		err := ValidateBracers(ass[i].vals)
		if ass[i].res {
			assert.Nil(t, err, i)
		} else {
			assert.NotNil(t, err, i)
		}
	}
}

func TestFull(t *testing.T) {
	ass := assertsCalculate()

	for i := range ass {
		res, err := Calculate(ass[i].vals, ass[i].in)
		if !ass[i].resErr {
			assert.Nil(t, err, i)
			assert.Equal(t, ass[i].res, res)
		} else {
			assert.NotNil(t, err, i)
		}

	}
}

func BenchmarkCalculate(b *testing.B) {
	ass := assertsCalculate()
	var x float64

	for i := 0; i < b.N; i++ {
		for i := range ass {
			x, _ = Calculate(ass[i].vals, ass[i].in)
		}
	}

	_ = x
}

type span struct {
	vals string
	res  string
}

func asserts() []span {
	return []span{
		{vals: "(2+1)*3", res: "2 1 + 3 *"},
		{vals: "(7+2) * 4 + 2", res: "7 2 + 4 * 2 +"},
		{vals: "4+13/5", res: "4 13 5 / +"},
		{vals: "(50-2) /12", res: "50 2 - 12 /"},
		{vals: "(4*2 / 4 * 25 - 2) / 12", res: "4 2 4 / 25 * * 2 - 12 /"},
		{vals: "(4*2 / 4 * 25 - 2) / 12 + 1000", res: "4 2 4 / 25 * * 2 - 12 / 1000 +"},
	}
}

type spanBracers struct {
	vals string
	res  bool
}

func assertsBracers() []spanBracers {
	return []spanBracers{
		{vals: "(2+1)*3", res: true},
		{vals: "(7+2) * 4 + 2", res: true},
		{vals: "4+13/5", res: true},
		{vals: "(50-2) /12", res: true},
		{vals: "((4+13/5", res: false},
		{vals: "(50-2)) /12", res: false},
		{vals: ")50-2) /12", res: false},
		{vals: ")50-2( /12", res: false},
		{vals: "(7+x) * yr + 2", res: true},
	}
}

type spanCalculate struct {
	vals   string
	in     map[string]float64
	res    float64
	resErr bool
}

func assertsCalculate() []spanCalculate {
	return []spanCalculate{
		{vals: "(2.0+1)*3", res: 9},
		{vals: "(7+xxx) *    weight + 2", in: map[string]float64{"xxx": 45.0}, res: 0, resErr: true},
		{vals: "(7+xxx) *    weight + 2", in: map[string]float64{"xxx": 45.0, "weight": 13}, res: 678},
		{vals: "4+13/5", res: 6.6},
		{vals: "amount*price*(1/(len/500*wid/1000))", in: map[string]float64{
			"len":    3000,
			"wid":    4000,
			"mod":    5.6,
			"price":  12,
			"amount": 1,
		}, res: 0.5},
		{vals: "(50-2) /12", res: 4},
		{vals: "(4*2 / 4 * 25 - 2) / 12", res: 4},
		{vals: "(4*2 / 4 * 25 - 2) / 12 + 1000", res: 1004},
	}
}
