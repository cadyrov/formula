package formula

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
)

/*
Count by user formulae
Uses Dejksta algo
*/
func Calculate(in string, attr map[string]float64) (float64, error) {
	if err := ValidateBracers(in); err != nil {
		return 0, err
	}

	return toPoland(in, attr)
}

const MiddleLengthVar = 10 //middle lenght value of char

var (
	ErrEmptyAttr      = errors.New("empty attribute")
	ErrZeroDivide     = errors.New("divide by zero")
	ErrBracersInvalid = errors.New("invalid bracers sequence")
)

func toPoland(in string, attr map[string]float64) (float64, error) {
	out := strings.Builder{}
	out.Grow(len(in))

	buf := make([]byte, 0, MiddleLengthVar)

	stOp := newBStack(len(in)/2 + 2) //stack for operators
	stFl := newFStack(len(in)/2 + 1) //stack for values
	var space, chars bool

	//функция для преобразования имен переменных в числа
	flush := func(validate bool) error {
		if len(buf) == 0 {
			return nil
		}

		name := string(buf)
		buf = buf[:0]
		chars = false

		if !validate {
			f, err := strconv.ParseFloat(name, 64)
			if err != nil {
				return err
			}

			stFl.Push(f)

			return nil
		}

		varf, ok := attr[name]
		if !ok {
			return fmt.Errorf("%s %w", name, ErrEmptyAttr)
		}

		stFl.Push(varf)

		return nil
	}

	calc := func(op byte) error {
		if op == '(' {
			return nil
		}

		b, _ := stFl.Pop()
		a, _ := stFl.Pop()
		switch op {
		case '*':
			stFl.Push(a * b)
		case '+':
			stFl.Push(a + b)
		case '/':
			if b == 0 {
				return ErrZeroDivide
			}
			stFl.Push(a / b)
		case '-':
			stFl.Push(a - b)
		case '^':
			stFl.Push(math.Pow(a, b))
		}

		return nil
	}

	for i := range in {
		if in[i] == ' ' {
			continue
		}

		if in[i] == '(' {
			if err := flush(chars); err != nil {
				return 0, err
			}

			stOp.Push(in[i])

			space = true

			continue
		}

		if in[i] == '/' || in[i] == '*' || in[i] == '-' || in[i] == '+' ||
			in[i] == '^' || in[i] == ')' {
			if err := flush(chars); err != nil {
				return 0, err
			}

			space = true

			for stOp.Size() > 0 {
				x, _ := stOp.Pop()

				if priority(x) > priority(in[i]) {
					if err := calc(x); err != nil {
						return 0, err
					}
				} else {
					stOp.Push(x)

					break
				}
			}

			if in[i] != ')' {
				stOp.Push(in[i])
			}

			continue
		}

		if space {
			if err := flush(chars); err != nil {
				return 0, err
			}
		}

		if in[i] >= 97 && in[i] < 123 {
			chars = true
		}

		buf = append(buf, in[i])

		space = false
	}

	if err := flush(chars); err != nil {
		return 0, err
	}

	for stOp.Size() > 0 {
		x, _ := stOp.Pop()

		if err := calc(x); err != nil {
			return 0, err
		}
	}

	res, _ := stFl.Pop()

	return res, nil
}

// inner chars asci from ( to 9 , " " , from a to z
func ValidateBracers(in string) error {
	st := newBStack(len(in) / 2)

	for i := range in {
		if !((in[i] >= 40 && in[i] < 58) ||
			(in[i] >= 97 && in[i] < 123) ||
			in[i] == 32) {
			return ErrBracersInvalid
		}

		if in[i] == '(' {
			st.Push(in[i])
		}

		if in[i] == ')' {
			if _, err := st.Pop(); err != nil {
				return ErrBracersInvalid
			}
		}
	}

	if st.Size() > 0 {
		return ErrBracersInvalid
	}

	return nil
}

func priority(in byte) uint8 {
	switch in {
	case '^':
		return 5
	case '/':
		return 4
	case '*':
		return 3
	case '-':
		return 2
	case '+':
		return 1
	default:
		return 0
	}
}
