package formula

import "errors"

var ErrEmpty = errors.New("empty stack")

type bStack struct {
	arr []byte
	idx int
}

func newBStack(len int) bStack {
	return bStack{
		arr: make([]byte, len),
	}
}

func (s *bStack) Push(in byte) {
	if len(s.arr)-1 < s.idx {
		s.arr = append(s.arr, in)
	} else {
		s.arr[s.idx] = in
	}

	s.idx++
}

func (s *bStack) Pop() (byte, error) {
	if s.idx == 0 {
		return 0, ErrEmpty
	}

	x := s.arr[s.idx-1]

	s.idx--

	return x, nil
}

func (s *bStack) Size() int {
	return s.idx
}

func (s *bStack) Reset() {
	s.idx = 0
}

type fStack struct {
	arr []float64
	idx int
}

func newFStack(len int) fStack {
	return fStack{
		arr: make([]float64, len),
	}
}

func (s *fStack) Push(in float64) {
	if len(s.arr)-1 < s.idx {
		s.arr = append(s.arr, in)
	} else {
		s.arr[s.idx] = in
	}

	s.idx++
}

func (s *fStack) Pop() (float64, error) {
	if s.idx == 0 {
		return 0, ErrEmpty
	}

	x := s.arr[s.idx-1]

	s.idx--

	return x, nil
}

func (s *fStack) Size() int {
	return s.idx
}

func (s *fStack) Reset() {
	s.idx = 0
}
